﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class PatientTestData {
        public long patientId { get; set; }
        public string testName { get; set; }
        public int testTypeId { get; set; }
        public int testScore { get; set; }
        public int testTime { get; set; }
        public int steps { get; set; }
        public int siteId { get; set; }
    }
}