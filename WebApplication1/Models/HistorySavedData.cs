﻿
using System.Collections.Generic;

namespace SelfitManagerAPI.Models
{
	public class HistorySavedData
	{
        public long plan_user { get; set; }
        public int site_id { get; set; }

        public List<SinglePlanInfo> plans { get; set; }

        public HistorySavedData(long plan_user, int site_id)
        {
            this.plan_user = plan_user;
            this.site_id = site_id;
            this.plans = new List<SinglePlanInfo>();
        }

        public void AddPlan(PlanInfo i_PlanInfo, List<PlanData> i_PlanPractices)
        {
            plans.Add(new SinglePlanInfo(i_PlanInfo, i_PlanPractices));
        }
    }

    public class SinglePlanInfo
    {
        public int plan_id { get; set; }
        public string plan_name { get; set; }

        public string plan_comment { get; set; }
        public string creation_date { get; set; }

        public int overall_practices_num { get; set; }
        public int overall_reps_num { get; set; }

        public List<SinglePracticeInPlan> practices { get; set; }

        public SinglePlanInfo(PlanInfo i_PlanInfo, List<PlanData> i_PlanPractices)
        {
            this.plan_id = i_PlanInfo.plan_number;
            this.plan_name = i_PlanInfo.plan_name;
            this.plan_comment = i_PlanInfo.plan_comment;
            this.creation_date = i_PlanInfo.creation_date;

            practices = new List<SinglePracticeInPlan>();

            overall_practices_num = i_PlanPractices.Count;
            overall_reps_num = 0;

            foreach (PlanData practiceInPlan in i_PlanPractices)
            {
                overall_reps_num += practiceInPlan.reps;
                practices.Add(new SinglePracticeInPlan(practiceInPlan));
            }
        }
    }

    public class SinglePracticeInPlan
    {
        public int practice_id { get; set; }
        public string practice_name { get; set; }
        public int reps { get; set; }

        public SinglePracticeInPlan(PlanData i_Practice)
        {
            this.practice_id = i_Practice.practice_id > 0 ? i_Practice.practice_id : i_Practice.practice_unity_id;
            this.practice_name = i_Practice.practice_name;
            this.reps = i_Practice.reps;
        }
    }
}