﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models
{
    public class HistoryCompletedData
    {
        public long user_id { get; set; }
        public int site_id { get; set; }

        public List<CompletedSession> completedSessions { get; set; }

        public HistoryCompletedData(long i_UserId, int i_SiteId) { 
            this.user_id = i_UserId;
            this.site_id = i_SiteId;
            this.completedSessions = new List<CompletedSession>();
        }

        public void AddCompletedSession(SessionData i_SessionData, SessionInfo i_SessionInfo, List<SessionPractices> i_SessionPractices)
        {
            completedSessions.Add(new CompletedSession(i_SessionData, i_SessionInfo, i_SessionPractices));
        }
    }

    public class CompletedSession
    {
        public int overall_practices_num { get; set; }
        public int overall_reps_num { get; set; }
        public SessionGeneralData sessionGeneralData { get; set; }
        public SessionRecords sessionRecords { get; set; }
        public List<SessionActualPractice> sessionPractices { get; set; }

        public CompletedSession(SessionData i_SessionData, SessionInfo i_SessionInfo, List<SessionPractices> i_SessionPractices)
        {
            this.sessionGeneralData = new SessionGeneralData(i_SessionData);
            this.sessionRecords = new SessionRecords(i_SessionInfo);
            this.sessionPractices = SessionActualPractice.ConvertList(i_SessionPractices);

            this.overall_practices_num = sessionPractices.Count;

            this.overall_reps_num = 0;

            foreach(SessionActualPractice practice in this.sessionPractices)
            {
                this.overall_reps_num += practice.reps;
            }
        }
    }

    public class SessionGeneralData
    {
        public int session_id { get; set; }
        public string plan_name { get; set; }
        public string start_time { get; set; }
        public int plan_id { get; set; }
        public string duration { get; set; }
        public string plan_comment { get; set; }
        public string creation_date { get; set; }

        public SessionGeneralData(SessionData i_SessionData)
        {
            this.session_id = i_SessionData.session_id;
            this.plan_name = i_SessionData.plan_name;
            this.start_time = i_SessionData.start_time;
            this.plan_id = i_SessionData.plan_id;
            this.duration = i_SessionData.duration;
            this.plan_comment = i_SessionData.plan_comment;
            this.creation_date = i_SessionData.creation_date;
        }
    }

    public class SessionRecords
    {
        public string Date { get; set; } 
        public float Total_Distance { get; set; } 
        public int Step_Count { get; set; } 
        public float Session_Time_gross { get; set; } 
        public float Speed { get; set; } 
        public float Step_hight_Right { get; set; } 
        public float Step_hight_Left { get; set; } 
        public float Step_Length_Right { get; set; } 
        public float Step_Length_Left { get; set; } 
        public float Movement_Time { get; set; } 

        public SessionRecords(SessionInfo i_SessionInfo)
        {
            this.Date = i_SessionInfo.Date;
            this.Total_Distance = i_SessionInfo.Total_Distance;
            this.Step_Count = i_SessionInfo.Step_Count;
            this.Session_Time_gross = i_SessionInfo.Session_Time_gross;
            this.Speed = i_SessionInfo.Speed;
            this.Step_hight_Right = i_SessionInfo.Step_hight_Right;
            this.Step_hight_Left = i_SessionInfo.Step_hight_Left;
            this.Step_Length_Right = i_SessionInfo.Step_Length_Right;
            this.Step_Length_Left = i_SessionInfo.Step_Length_Left;
            this.Movement_Time = i_SessionInfo.Movement_Time;
        }
    }

    public class SessionActualPractice
    {
        public int practice_id { get; set; }
        public int unity_id { get; set; }
        public string practice_name { get; set; }
        public int reps { get; set; }

        public SessionActualPractice(SessionPractices i_SingleSessionPractice) {
            this.practice_id = i_SingleSessionPractice.practice_id;
            this.unity_id = i_SingleSessionPractice.unity_id;
            this.practice_name = i_SingleSessionPractice.practice_name;
            this.reps = i_SingleSessionPractice.reps;
        }

        public static List<SessionActualPractice> ConvertList(List<SessionPractices> i_ListOfSessionPractices)
        {
            List<SessionActualPractice> practices = new List<SessionActualPractice>();

            foreach(SessionPractices practice in i_ListOfSessionPractices)
            {
                practices.Add(new SessionActualPractice(practice));
            }

            return practices;
        }
    }
}