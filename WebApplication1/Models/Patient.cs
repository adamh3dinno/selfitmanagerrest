﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class Patient {        
        public long userId { get; set; }
        public int lang { get; set; }
        public int gender { get; set; }
        public int birthYear { get; set; }
        public int height { get; set; }
        public int weight { get; set; }        
        public String status_summarey { get; set; }
        public String notes { get; set; }
        public int shueSize { get; set; }
        public int siteId { get; set; }
    }
}