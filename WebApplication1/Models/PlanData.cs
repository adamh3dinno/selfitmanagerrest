﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class PlanData {
        public int plan_id { get; set; }
        public int plan_number { get; set; }
        public int practice_id { get; set; }
        public string practice_name { get; set; }
        public int practice_unity_id { get; set; }
        public int order_num { get; set; }
        public long plan_user { get; set; }
        public int site_id { get; set; }
        public int reps { get; set; }
    }
    public class PlanDataSlim
    {
        public int practice_id { get; set; }
        public int order_num { get; set; }
        public int reps { get; set; }

        public static PlanDataSlim GetPlanDataSlim(PlanData planData)
        {
            PlanDataSlim slim = new PlanDataSlim();

            int practice_id = planData.practice_id > 0 ? planData.practice_id : planData.practice_unity_id;

            slim.practice_id = practice_id;
            slim.order_num = planData.order_num;
            slim.reps = planData.reps;

            return slim;
        }

        public static List<PlanDataSlim> GetPlanDataSlimList(List<PlanData> listOfPlanData)
        {
            List<PlanDataSlim> planDataSlimList = new List<PlanDataSlim>();

            foreach(PlanData planData in listOfPlanData) {
                PlanDataSlim planDataSlim = PlanDataSlim.GetPlanDataSlim(planData);
                planDataSlimList.Add(planDataSlim);
            }

            return planDataSlimList;
        }
    }

    public class PlanDataSlimWithAdditionalInfo
    {
        public int plan_id { get; set; }
        public string plan_comment { get; set; }
        public bool new_plan_required { get; set; }
        public List<PlanDataSlim> planDataSlim { get; set; }

        public PlanDataSlimWithAdditionalInfo(int plan_id, string plan_comment, bool new_plan_required, List<PlanDataSlim> planDataSlim)
        {
            this.plan_id = plan_id;
            this.plan_comment = plan_comment;
            this.new_plan_required = new_plan_required;
            this.planDataSlim = planDataSlim;
        }
    }

    public class PlanInfo {
        public int plan_number { get; set; }
        public string plan_name { get; set; }
        public long plan_user { get; set; }
        public int site_id { get; set; }
        public int count { get; set; }
        public string plan_comment { get; set; }
        public string creation_date { get; set; }
    }
    public class PlanSummary {
        public int plan_count { get; set; }
        public int reps { get; set; }
    }
}