﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class SessionInfo {
        public int session_id { get; set; } //0
        public long user_id { get; set; } //1
        public int site_id { get; set; } //2
        public string Date { get; set; } //3
        public float Total_Distance { get; set; } //4
        public int Step_Count { get; set; } //5
        public float Session_Time_gross { get; set; } //6
        public float Speed { get; set; } //7
        public float Step_hight_Right { get; set; } //8
        public float Step_hight_Left { get; set; } //9
        public float Step_Length_Right { get; set; } //10
        public float Step_Length_Left { get; set; } //11
        public float Movement_Time { get; set; } //12
    }
}