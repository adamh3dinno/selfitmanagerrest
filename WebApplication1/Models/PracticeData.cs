﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class PracticeData {
        public int practice_id { get; set; }
        public string practice_name { get; set; }
        public int unity_id { get; set; }
    }
    public class ImageData {
        public int practice_id { get; set; }
        public int image_id { get; set; }
    }

    public class PracticeInfo {
        public int practice_id { get; set; }
        public string practice_description { get; set; }
        public string main_focus { get; set; }
        //public string practice_instructions { get; set; }
        //public string changeable_parameters { get; set; }
        //public string physical_benefits { get; set; }
    }
}