﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class GeneralReportData {
        public int session_id { get; set; }
        public long user_id { get; set; }
        public int exercises { get; set; }
        public string date { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public int duration { get; set; }
        public int site_id { get; set; }
    }
    public class SummaryReportData {
        public int session_count { get; set; }
        public int patients { get; set; }
        public int total_exercises { get; set; }
        public int total_seconds { get; set; }
        public string date { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public int total_months { get; set; }
        public string completion_percentage { get; set; }
    }
    public class GeneralInfo {
        public int active_sites { get; set; }
        public int total_patients { get; set; }
        public int active_plans { get; set; }
        public int performedSessions { get; set; }
        public int performedExercises { get; set; }
        public int totalSteps { get; set; }
        public string TotalTime_gross { get; set; }
        public float Total_Distance { get; set; }
        public int TotalSystemExecrsises { get; set; }
    }
    public class PerformedPractices {
        public int practice_id { get; set; }
        public string practiceName { get; set; }
        public int performed { get; set; }
    }

    public class PracticeMeasures {
        public int practice_id { get; set; }
        public int measure_id { get; set; }
        public string measure_name { get; set; }
    }

    public class PracticeMeasuresLog {        
        public int practice_id { get; set; } // 0
        public string practiceName { get; set; } // 1
        public int practiceNumber { get; set; } // 2
        public int measureId { get; set; } // 3
        public string measureName { get; set; } // 4
        public float measureScoreLog { get; set; } // 5
        public int sessionId { get; set; } // 8
        public string practiceTime { get; set; } //9
    }

    class SessionDone {

        public SessionDone(int session_id, int plan_id) {
            this.session_id = session_id;
            this.plan_id = plan_id;
        }

        public int session_id;
        public int plan_id;
    }

    class PracticeAndRepsInPlan {

        public PracticeAndRepsInPlan(int practice_id, int reps) {
            this.practice_id = practice_id;
            this.reps = reps;
        }

        public int practice_id;
        public int reps;

    }

    class PlanInfoWithAllPracticesAndReps {

        public PlanInfoWithAllPracticesAndReps(int plan_id) {
            this.plan_id = plan_id;
        }

        public int plan_id;
        public List<PracticeAndRepsInPlan> allPractices = new List<PracticeAndRepsInPlan>();

    }

    class SessionPracticesLog {

        public SessionPracticesLog(int session_id) {
            this.session_id = session_id;
        }

        public int session_id;
        public int plan_id;
        public List<int> practice_ids = new List<int>();
        
    }

    public class PatientOverviewData {
        public int system_duration;
        public int num_of_sessions;
        public string avg_session_time;
        public string avg_practices_session;
        public string avg_practice_time;
        public string completion_percentage;
    }
}