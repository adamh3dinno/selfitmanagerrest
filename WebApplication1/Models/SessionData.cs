﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfitManagerAPI.Models {
    public class SessionData {
        //SELECT MAX(sessions.session_id), MAX(plans_info.plan_name), CAST(MAX(sessions.start_time) AS DATE) AS Date, " +
        //"COUNT(sessions.session_id) as Practices, MAX(sessions.plan_id)
        public long userId { get; set; }
        public int siteId { get; set; }
        public int session_id { get; set; }
        public string plan_name { get; set; }
        public string start_time { get; set; }
        public int practice_count { get; set; }
        public int plan_id { get; set; }
        public string duration { get; set; }
        public string plan_comment { get; set; }
        public string creation_date { get; set; }
    }

    public class SessionPractices {
        public long userId { get; set; }
        public int siteId { get; set; }
        public int session_id { get; set; }
        public int practice_id { get; set; }
        public int unity_id { get; set; }
        public string practice_name { get; set; }
        public int reps { get; set; }
    }

    public class ParkinsonSessionData {
        public int practice_id { get; set; }
        public int PracticeStage { get; set; }
        public int stepCount { get; set; }
        public float AvgStepTotalLength { get; set; }
        public float StdDeviation { get; set; }
        public float StepTime { get; set; }
    }
    public class ParkinsonStepsData {
        public int step_type { get; set; }
        public float step_length { get; set; }
        public float step_time { get; set; }
        public int pratice_stage { get; set; }
        public int practice_pass { get; set; }
        public int step_num_in_stage { get; set; }
        public int stage_type { get; set; }
    }

    public class TaskData {
        public int taskId {get; set;}
        public int state { get; set; }
        public string task_data { get; set; }
        public string task_info { get; set; }
    }

}