﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApplication1
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Cors
            //var cors = new EnableCorsAttribute(origins: "http://localhost:5050,http://ec2-18-225-11-66.us-east-2.compute.amazonaws.com:5050", headers: "*", methods: "*"); // origins, headers, methods

            var cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*"); // origins, headers, methods

            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
