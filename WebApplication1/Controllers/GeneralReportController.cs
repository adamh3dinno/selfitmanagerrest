﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class GeneralReportController : ApiController
    {
        // GET: api/GeneralReport?from=<string>&to=<string>&patientID=<string>&site=<int>
        public ArrayList Get(string from, string to, string patientID, int site)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getGeneralReportDataFiltered(from, to, patientID, site);
        }

        
    }
}
