﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class PracticeImagesController : ApiController
    {
        // GET: api/PracticeImages?siteId=<int>
        public ArrayList Get(int siteId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getImageNames(siteId);
        }
        
    }
}
