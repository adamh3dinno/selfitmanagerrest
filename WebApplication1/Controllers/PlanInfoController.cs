﻿using SelfitManagerAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class PlanInfoController : ApiController
    {
        // GET: api/PlanInfo?patientId=<long>
        public int Get(long patientId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPatientPlanCount(patientId);
        }

        // GET: api/PlanInfo?patientId=<long>&siteId=<int>
        public List<PlanInfo> Get(long patient, int site)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getUserPlansInfo(patient, site);
        }        
    }
}
