﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class userTestsController : ApiController
    {
        // GET: api/userTests?pid=<long>&site=<int>
        public ArrayList Get(long pId, int site)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getPatientTestData(pId, site);            
        }

        // POST: api/userTests?pId=<long>&site=<int> + [FromBody] PatientTestData
        [HttpPost]
        public int Post([FromBody] PatientTestData testData, long pId, int site)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            int hasInserted = -1;
            bool inserted = dbh.addPatientTestData(testData, pId, site);
            
            if (inserted) {
                hasInserted = 0;
            }            
            return hasInserted;
        }       
        
    }
}
