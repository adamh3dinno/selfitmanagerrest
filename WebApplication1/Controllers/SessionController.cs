﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Models
{
    public class SessionController : ApiController
    {
        // GET: api/Session?pID=<long>&site=<int>
        public List<SessionData> Get(long patientId, int siteID)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getUserSessionInfo(patientId, siteID); ;
        }

        // GET: api/Session?pID=<long>&site=<int>&startDate=<string>&endDate=<string>
        public ArrayList Get(long pId, int site, string startDate, string endDate)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getUserSessionInfo(pId, site, startDate, endDate);            
        }        
        
    }
}
