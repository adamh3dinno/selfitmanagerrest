﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class ReportController : ApiController {

        // GET: api/Report
        public ArrayList Get() {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPracticeSupportedMeasures();
        }

        // GET: api/Report?langStr=<string>
        public ArrayList Get(string langStr) {
            DataBaseHandler mdb = new DataBaseHandler();

            int langId = 0;
            if (langStr != null)
                langId = mdb.getLangIdFromStr(langStr);

            return mdb.getPracticeSupportedMeasures(langId);
        }

        //// GET: api/Report?pId=<long>&site=<int>&langStr=<string>
        public ArrayList Get(long pId, int site, string langStr = "en") {

            DataBaseHandler mdb = new DataBaseHandler();
            int langId = 0;
            if (langStr != null)
                langId = mdb.getLangIdFromStr(langStr);

            return mdb.getPerformedPractices(pId, site, langId);
        }


        // this is for the old wpf manager system
        //// GET: api/Report?patientId=<long>&siteId=<int>&practiceId=<int>
        public ArrayList Get(long patientId, int siteId, int practiceId) {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPracticeMeasuresData(patientId, siteId, practiceId);
        }

        // this is for the new react manager system
        //// GET: api/Report?patientId=<long>&siteId=<int>&practiceId=<int>&langStr=<string>
        public ArrayList Get(long patientId, int siteId, int practiceId, string langStr) {
            DataBaseHandler mdb = new DataBaseHandler();

            int langId = 0;
            if (langStr != null)
                langId = mdb.getLangIdFromStr(langStr);

            return mdb.getPracticeMeasuresDataWithLang(patientId, siteId, practiceId, langId);
        }
    }
}
