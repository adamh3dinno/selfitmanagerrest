﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class GeneralInfoController : ApiController
    {
        // GET: api/GeneralInfo
        public GeneralInfo Get() {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getGeneralInfo();
        }
    }
}
