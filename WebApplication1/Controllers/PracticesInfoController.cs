﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class PracticesInfoController : ApiController
    {

        // GET: api/PracticesInfo?siteId=<int>&langStr=<string>
        public ArrayList Get(int siteId, string langStr)
        {
            DataBaseHandler mdb = new DataBaseHandler();

            int langId = 0;
            
            if(langStr  != null)
                langId = mdb.getLangIdFromStr(langStr);

            return mdb.getPracticesInfo(siteId, langId);
        }


    }
}
