﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class PlanCommentController : ApiController
    {

        // GET: api/PlanComment?planId=<int>&siteId=<int>
        public string Get(int planId, int siteId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPlanComment(planId, siteId);
            
        }

        // POST: api/PlanComment?planId=<int>&siteId=<int>&comment=<string>
        public bool Post(int planId, int siteId, string comment)
        {

            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.postPlanComment(planId, siteId, comment);
        }

    }
}
