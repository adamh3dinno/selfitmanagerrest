﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class TasksController : ApiController
    {
        public TaskData Get(int site, int machineId) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getTaskData(site, machineId);
        }

        public TaskData Post(int site, int machineId, string task_info) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.setTaskData(site, machineId, task_info);
        }

        public int Put(int site, int machineId, int taskId) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.setTaskCompleted(site, machineId, taskId);
        }
    }
}
