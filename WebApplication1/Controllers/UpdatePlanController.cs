﻿using SelfitManagerAPI.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class UpdatePlanController : ApiController
    {

        [Route("api/UpdatePlan")]
        [HttpPost]
        // POST: api/UpdatePlan?plan_id=<int>&patient_id=<long>&site_id=<int>&plan_comment=<string>&force_create=<boolean> + [FromBody] List<PlanData>
        public PlanDataSlimWithAdditionalInfo Post(int plan_id, int patient_id, int site_id, string plan_comment, bool force_create, [FromBody] List<PlanDataSlim> value)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.updatePlan(plan_id, patient_id, site_id, plan_comment, force_create, value);
        }

    }
}
