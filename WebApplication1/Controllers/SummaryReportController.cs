﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class SummaryReportController : ApiController
    {
        // GET: api/SummaryReport?fromDate=<string>&toDate=<string>&userIdStr=<string>&siteId=<int>
        public ArrayList Get(string fromDate, string toDate, string userIdStr, int siteId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getSummaryReportDataFiltered(fromDate, toDate, userIdStr, siteId);
        }

        // GET: api/SummaryReport?userIdStr=<string>&siteId=<int>
        public ArrayList Get(string userIdStr, int siteId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPatientSummaryReportData(userIdStr, siteId);
        }

        
    }
}
