﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class PlanController : ApiController
    {
        // GET: api/Plan?planId=<int>&siteId=<int>&langStr=<string>
        public List<PlanData> Get(int planId, int siteId, string langStr = "en")
        {
            DataBaseHandler mdb = new DataBaseHandler();

            int langId = 0;
            if (langStr != null)
                langId = mdb.getLangIdFromStr(langStr);

            return mdb.getUserPlanData(planId, siteId, langId);
        }

        // POST: api/Plan?patient=<long>&site=<int>&plan_name=<string> + [FromBody] List<PlanData>
        public int Post(long patient, int site, string plan_name,  [FromBody] List<PlanData> value)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.storePatientPlan(patient, site, plan_name, PlanDataSlim.GetPlanDataSlimList(value), "");
        }

        // POST: api/Plan?patient=<long>&site=<int>&plan_name=<string>&comment=aaa + [FromBody] List<PlanData>
        public int Post(long patient, int site, string plan_name, string comment, [FromBody] List<PlanDataSlim> value)
        {
            if (comment == null)
                comment = "";

            if (plan_name == null || plan_name.Equals(String.Empty) || plan_name.Equals(""))
                plan_name = "Plan ";

            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.storePatientPlan(patient, site, plan_name, value, comment);
        }
    }
}
