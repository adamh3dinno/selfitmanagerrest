﻿using SelfitManagerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class SessionDataController : ApiController
    {
        // GET: api/SessionData?patientID=<long>&sessionID=<int>&siteID=<int>
        public SessionInfo Get(long patientID, int sessionID, int siteID) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getPatientSessionInfo(patientID, sessionID, siteID);
        }

    }
}
