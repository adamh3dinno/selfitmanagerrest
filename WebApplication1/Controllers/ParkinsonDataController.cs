﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class ParkinsonDataController : ApiController
    {
        public ArrayList Get(long userId, int siteId, int sessionId) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getParkinsonSessionInfo(userId, siteId, sessionId);
        }

    }
}
