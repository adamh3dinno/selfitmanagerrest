﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class PatientController : ApiController
    {
        // GET: api/Patient?pId=<long>&site=<int> -- will return full patient information
        public Patient Get(long pId, int site)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getPatientsData(pId, site);
        }

        // GET: api/Patient?siteId=<int>   -- will return the patient count for site     
        public int Get(int siteId) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getPatientsCount(siteId);
        }

        // POST: api/Patient?site=<int> + [FromBody] Patient
        [HttpPost]
        public long Post(int site, [FromBody] Patient value)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.addPatient(site, value);
           
        }

        // PUT: api/Patient?id=<long>&site=<int> + [FromBody] Patient
        [HttpPut]
        public int Put(long id, int site, [FromBody] Patient value)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            bool recordExisted = false;
            recordExisted = dbh.updatePatient(value, id, site);
            int existed = -1;
            
            if (recordExisted) {
                existed = 0;
            }            
            return existed;
        }
        
    }
}
