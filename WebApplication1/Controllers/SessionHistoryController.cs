﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class SessionHistoryController : ApiController
    {
        // GET: api/SessionHistory?userId=<long>&siteId=<int>&DataLimit=<int>&sessionId=<int>
        public ArrayList Get(long userId, int siteId, int DataLimit, int sessionId)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getUserSessionInfo(userId, siteId, DataLimit, sessionId);
        }

        // GET: api/SessionHistory?siteId=<int>&sessionId=<int>
        public List<SessionPractices> Get(int site_id, int session) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getSessionPractices(site_id, session);
        }
    }
}
