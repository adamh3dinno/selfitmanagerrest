﻿using SelfitManagerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class WakeUpController : ApiController
    {
        [Route("api/wakeup")]
        [HttpGet]
        public int Get()
        {
            return 1;
        }

    }
}
