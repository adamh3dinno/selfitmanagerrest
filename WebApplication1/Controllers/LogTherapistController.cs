﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class LogTherapistController : ApiController
    {
        // GET: api/LogTherapist
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //this is the new login for therapist user.
        // GET: api/LogTherapist?name=<string>&psw=<string>
        public int Get(string name, string psw) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.TherapistUserLogin(name, psw);
        }

        // POST: api/LogTherapist
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LogTherapist/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LogTherapist/5
        public void Delete(int id)
        {
        }
    }
}
