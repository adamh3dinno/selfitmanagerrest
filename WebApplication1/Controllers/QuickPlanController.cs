﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class QuickPlanController : ApiController
    {


        // GET: (int - program number) api/QuickPlan?patient=long&site=int&totalTime=int&standing=bool&sitting=bool [FromBody] List<string> categories
        public int Get(long patient, int site, int totalTime, bool standing, bool sitting, [FromUri] List<string> categories)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getQuickPlan(patient, site, totalTime, standing, sitting, categories);
        }
    }
}
