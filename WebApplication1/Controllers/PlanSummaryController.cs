﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SelfitManagerAPI.Models;

namespace SelfitManagerAPI.Controllers
{
    public class PlanSummaryController : ApiController
    {
        // GET: api/PlanSummary?patientId<long>&siteId=<int>
        public PlanSummary Get(long patientId, int siteId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPatientPlanSummaryData(patientId, siteId);
        }

        
    }
}
