﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SelfitManagerAPI.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TherapistController : ApiController
    {
        // GET: api/Therapist
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //this is the old login to the therapist user. used in old windows forms manager
        // GET: api/Therapist?name=<string>&psw=<string>&site=<string>
        public int Get(string name, string psw, string site)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getUserRights(name, psw, site);
        }

        
    }
}
