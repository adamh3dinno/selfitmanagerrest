﻿using SelfitManagerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class PatientOverviewController : ApiController
    {

        // GET: api/PatientOverview?userId=<int>&siteId=<int>
        public PatientOverviewData Get(int userId, int siteId)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getPatientOverviewData(userId, siteId);
        }
        
    }
}
