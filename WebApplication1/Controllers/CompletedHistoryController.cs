﻿using SelfitManagerAPI.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class CompletedHistoryController : ApiController
    {

        [Route("api/CompletedHistory")]
        [HttpGet]
        // GET: api/CompletedHistory?patient_id=123&site_id=1
        public HistoryCompletedData Get(long patient_id, int site_id)
        {
            DataBaseHandler dbh = new DataBaseHandler();
            HistoryCompletedData historyCompletedData = new HistoryCompletedData(patient_id, site_id);

            List<SessionData> sessionDataList = dbh.getUserSessionInfo(patient_id, site_id);

            foreach (SessionData sessionData in sessionDataList)
            {
                SessionInfo sessionInfo = dbh.getPatientSessionInfo(sessionData.userId, sessionData.session_id, sessionData.siteId);
                List<SessionPractices> sessionPractices =  dbh.getSessionPractices(sessionData.siteId, sessionData.session_id);

                historyCompletedData.AddCompletedSession(sessionData, sessionInfo, sessionPractices);
            }

            return historyCompletedData;
        }

    }
}
