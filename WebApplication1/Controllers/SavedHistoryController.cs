﻿using SelfitManagerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Numerics;
using System.Security.Policy;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class SavedHistoryController : ApiController
    {

        // GET: api/SavedHistory?patient_id=123&site_id=1&lang_str=en
        [Route("api/SavedHistory")]
        [HttpGet]
        public HistorySavedData Get(long patient_id, int site_id, string lang_str)
        {

            DataBaseHandler mdb = new DataBaseHandler();

            int langId = 0;
            if (lang_str != null)
                langId = mdb.getLangIdFromStr(lang_str);

            HistorySavedData historySavedData = new HistorySavedData(patient_id, site_id);

            List<PlanInfo> allPlansList = mdb.getUserPlansInfo(patient_id, site_id);

            foreach(PlanInfo plan in allPlansList) {
                List<PlanData> practicesInPlan = mdb.getUserPlanData(plan.plan_number, site_id, langId);

                historySavedData.AddPlan(plan, practicesInPlan);
            }

            return historySavedData;
        }

    }
}
