﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class ParkinsonStepsDataController : ApiController
    {
        public ArrayList Get(long userId, int siteId, int sessionId) {
            DataBaseHandler dbh = new DataBaseHandler();
            return dbh.getParkinsonStepsData(userId, siteId, sessionId);
        }
    }
}
