﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SelfitManagerAPI.Controllers
{
    public class PracticeDataController : ApiController
    {
        // GET: api/PracticeData?langStr=<string>
        public ArrayList Get(string langStr = "en")
        {
            DataBaseHandler mdb = new DataBaseHandler();
            int langId = 0;
            if (langStr != null)
                langId = mdb.getLangIdFromStr(langStr);

            return mdb.getAllActivePractices(langId);
        }

        // GET: api/PracticeData?position=<int> [FromBody] List<string> filters
        public ArrayList Get(int position, [FromUri] List<string> filters)
        {
            DataBaseHandler mdb = new DataBaseHandler();
            return mdb.getFilterActivePractices(position, filters);
        }

        
    }
}
