﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCrypt.Net;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using SelfitManagerAPI.Models;
using System.Collections;
using System.Data;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Security.Policy;

namespace SelfitManagerAPI {
    public class DataBaseHandler {

        public int getLangIdFromStr(string langStr) {
            langStr = langStr.ToLower();

            if(langStr.Equals("en") || langStr.Equals("eng")|| langStr.Equals("english")) {
                return 0;
            }
            else if(langStr.Equals("he") || langStr.Equals("heb") || langStr.Equals("hebrew")) {
                //return 1;
                //no hebrew. return default
            }
            else if(langStr.Equals("gi") || langStr.Equals("gib") || langStr.Equals("gibberish")) {
                return 2;
            }

            return 0;//english is defualt in system
        }

        private void addExceptionLog(string query,
                                     string info) {

            int key = getMaxExceptionLogKey();
            key++;

            query = query.Replace("'", "''");
            info = info.Replace("'", "''");

            string insert = " INSERT INTO exception_logs VALUES("
                            + key + ", GETDATE(), "
                            + " '" + query + "' ,"
                            + " '" + info + "' ,"
                            + /*requestInfo.site_id*/ 0 + ", "
                            + /*requestInfo.machine_id*/ 0 + ", "
                            + /*requestInfo.user_id*/ 0 + ")";

            SqlConnection conn = new SqlConnection();

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand(insert, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("Error throwen: " + ex);
                System.Diagnostics.Debug.WriteLine("addExceptionLog: " + insert);
            }
            finally {
                conn.Close();
            }
        }

        private int getMaxExceptionLogKey() {

            SqlConnection conn = new SqlConnection();

            int max = 0;
            try {
                string query = "SELECT MAX([key]) FROM exception_logs";

                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();

                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        max = (int)SQLReader[0];
                    }
                }
                SQLReader.Dispose();
            }

            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("Error throwen: " + ex);
                //cannot add exceptions here because table must have key number
            }
            finally {
                conn.Close();
            }
            return max;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uName"></param>
        /// <param name="uPass"></param>
        /// <returns></returns>
        public int getUserRights(string uName, string uPass, string siteIdStr) {

            SqlConnection conn = new SqlConnection();
            int userId = -1;
            string query = "";

            try {
                int uId = 0;
                string psw = string.Empty;
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT id, password FROM [dbo].[Users] WHERE username = '" + uName +
                                /*"' AND password = '" + myHashedPass + */ "' AND site_id = " + siteIdStr;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        uId = (int)SQLReader[0];
                        psw = SQLReader[1].ToString();
                    }
                    if (psw != string.Empty & uId > 0) {
                        if (BCrypt.Net.BCrypt.Verify(uPass, psw)) {
                            userId = uId;
                        }
                    }
                }
            }
            catch (Exception ex) {
                addExceptionLog(query, "Manager|getUserRights: " + ex.Message);
                if (ex.Message != "Invalid salt version")
                    throw ex;
            }
            finally {
                conn.Close();
            }
            return userId;
        }

        public int TherapistUserLogin(string uName, string uPass) {
            SqlConnection conn = new SqlConnection();
            int userId = -1;
            int site_id = -1;
            string query = "";

            try {
                int uId = 0;
                string psw = string.Empty;
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT id, password, site_id FROM [dbo].[Users] WHERE username = '" + uName + "'";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        uId = (int)SQLReader[0];
                        psw = SQLReader[1].ToString();
                        site_id = int.Parse(SQLReader["site_id"].ToString());
                    }
                    if (psw != string.Empty & uId > 0) {
                        if (BCrypt.Net.BCrypt.Verify(uPass, psw)) {
                            userId = uId;
                        }
                    }
                }
            }
            catch (Exception ex) {
                addExceptionLog(query, "Manager|TherapistUserLogin: " + ex.Message);
                if (ex.Message != "Invalid salt version")
                    throw ex;
            }
            finally {
                conn.Close();
            }
            if (userId != -1)
                return site_id;
            else
                return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public Patient getPatientsData(long pId, long siteId) {

            SqlConnection conn = new SqlConnection();
            Patient pData = new Patient();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT * FROM [dbo].patient_info WHERE user_id = " + pId + " AND site_id = " + siteId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) {
                    pData.userId = SQLReader.GetInt32(0);
                    pData.lang = SQLReader.GetInt32(1);
                    pData.gender = SQLReader.GetInt32(2);
                    pData.birthYear = SQLReader.GetInt32(3);
                    pData.height = SQLReader.GetInt32(4);
                    pData.weight = SQLReader.GetInt32(5);
                    if (!SQLReader.IsDBNull(6))
                        pData.status_summarey = SQLReader.GetString(6);
                    if (!SQLReader.IsDBNull(7))
                        pData.notes = SQLReader.GetString(7);
                    pData.shueSize = SQLReader.GetInt32(10);
                    pData.siteId = SQLReader.GetInt32(11);
                    return pData;
                }
                else {
                    return null;
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientsData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public int getPatientsCount(int siteId) {
            SqlConnection conn = new SqlConnection();
            int uCount = 0;
            string query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT COUNT(user_id) FROM [dbo].patient_info WHERE site_id = " + siteId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        //MessageBox.Show("Count = " + count);
                        uCount = int.Parse(SQLReader[0].ToString());
                    }
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientsCount: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return uCount;
        }

        public long addPatient(int siteId, Patient pt) {


            SqlConnection conn = new SqlConnection();
            SqlDataReader SQLReader = null;
            string query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "SELECT * FROM [dbo].patient_info WHERE user_id= " + pt.userId + " AND site_id = " + siteId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (!SQLReader.Read()) { // If user not exits
                    SQLReader.Close();
                    query = "INSERT INTO [dbo].patient_info(user_id, lang_id, user_gender, user_byear, user_hight, user_whight, user_comments, user_shoe_size, site_id)"
                    + " VALUES (" + pt.userId + " ," + pt.lang + ", " + pt.gender + ",'" + pt.birthYear + "'," + pt.height + "," + pt.weight + ",'" + pt.notes + "', " + pt.shueSize + ", " + siteId + ")";
                    cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    return 0;
                }
                else {
                    return -1;
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|addPatient: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
        }

        public bool updatePatient(Patient pt, long pId, int site) {

            SqlConnection conn = new SqlConnection();
            SqlDataReader SQLReader = null;
            string query = "";
            //int level = 0;
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "SELECT * FROM [dbo].patient_info WHERE user_id= " + pId + " AND site_id = " + site;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) { // If user exits
                    SQLReader.Close();
                    query = "UPDATE [dbo].patient_info SET" +
                            " user_gender = " + pt.gender +
                            ", user_hight = " + pt.height +
                            ", user_whight = " + pt.weight +
                            ", user_comments = '" + pt.notes +
                            //"', user_level = " + level +
                            "', user_byear = '" + pt.birthYear +
                            "' , lang_id = " + pt.lang +
                            ", user_shoe_size = " + pt.shueSize +
                            " WHERE user_id = " + pId +
                            " AND site_id = " + site;
                    cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|updatePatient: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
        }

        public ArrayList getPatientTestData(long uId, int site) {

            SqlConnection conn = new SqlConnection();
            ArrayList pTestData = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT user_tests.time, test_types.test_name, test_score, test_time, test_steps " +
                        "FROM [dbo].user_tests, [dbo].test_types " +
                        "WHERE test_types.test_id = user_tests.test_type AND user_id = " + uId +
                        " AND site_id = " + site;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PatientTestData ptData = new PatientTestData();
                    ptData.patientId = uId;
                    ptData.siteId = site;
                    ptData.testTime = SQLReader.GetInt32(0);
                    ptData.testName = SQLReader.GetString(1);
                    ptData.testScore = SQLReader.GetInt32(2);
                    ptData.testTime = SQLReader.GetInt32(3);
                    ptData.steps = SQLReader.GetInt32(4);
                    pTestData.Add(ptData);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientTestData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return pTestData;
        }

        public bool addPatientTestData(PatientTestData td, long patientId, int site) {
            SqlConnection conn = new SqlConnection();
            SqlDataReader SQLReader = null;
            bool ret = false;
            string query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "INSERT INTO [dbo].user_tests(user_id, site_id, time, test_type, test_score, test_time, test_steps) " +
                    "VALUES (" + patientId + "," + site + ", NOW(), " + td.testTypeId + ", " + td.testScore + ", " + td.testTime + ", " + td.steps + ")";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
                ret = true;
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|addPatientTestData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return ret;
        }

        public List<SessionData> getUserSessionInfo(long uId, int siteId) {
            SqlConnection conn = new SqlConnection();
            List<SessionData> userSdata = new List<SessionData>();
            string query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT MAX(sessions.session_id), MAX(plans_info.plan_name), convert(varchar,MAX(sessions.start_time),4) AS Date, " +
                                " COUNT(sessions.session_id) as Practices, MAX(sessions.plan_id), sessions.start_time AS TS, " +
                                " MAX(DATEDIFF(ss, sessions.start_time, sessions.end_time)) as practice_time, MAX(plans_info.comment) AS plan_comment, " +
                                " MAX(plans_info.creation_date) AS creation_date " +
                                " FROM [dbo].sessions, [dbo].practice_log, [dbo].plans_info" +
                                " WHERE sessions.user_id = " + uId +
                                " AND sessions.site_id = " + siteId +
                                " AND sessions.session_id = practice_log.session_id" +
                                " AND sessions.plan_id = plans_info.plan_number" +
                                " AND practice_log.site_id = " + siteId +
                                " AND plans_info.site_id in (0, " + siteId + ")" +
                                " GROUP BY sessions.session_id, sessions.start_time ORDER BY TS DESC";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    SessionData sd = new SessionData();
                    sd.userId = uId;
                    sd.siteId = siteId;
                    sd.session_id = SQLReader.GetInt32(0);
                    sd.plan_name = SQLReader.GetString(1);
                    sd.start_time = SQLReader.GetString(2);
                    sd.practice_count = SQLReader.GetInt32(3);
                    sd.plan_id = SQLReader.GetInt32(4);

                    int practiceTimeSec = int.Parse(SQLReader["practice_time"].ToString());
                    sd.duration = secondsToStringTime(practiceTimeSec);

                    sd.plan_comment = SQLReader["plan_comment"].ToString();
                    sd.creation_date = SQLReader["creation_date"].ToString();

                    userSdata.Add(sd);

                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getUserSessionInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return userSdata;
        }

        public ArrayList getUserSessionInfo(long uId, int siteId, string startDateStr, string endDateStr) {
            SqlConnection conn = new SqlConnection();
            ArrayList uSessionInfo = new ArrayList();
            string query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT MAX(sessions.session_id), MAX(plans_info.plan_name), convert(varchar,MAX(sessions.start_time),4) AS Date, " +
                                "COUNT(sessions.session_id) as Practices, MAX(sessions.plan_id)" +
                                " FROM [dbo].sessions, [dbo].practice_log, [dbo].plans_info" +
                                " WHERE sessions.user_id = " + uId +
                                " AND sessions.site_id = " + siteId +
                                " AND sessions.session_id = practice_log.session_id" +
                                " AND sessions.plan_id = plans_info.plan_number" +
                                " AND practice_log.site_id = " + siteId +
                                " AND plans_info.site_id = (0, " + siteId + ")" +
                                " AND sessions.start_time > '" + startDateStr + "'" +
                                " AND sessions.start_time < '" + endDateStr + "'" +
                                " GROUP BY sessions.session_id ORDER BY Date DESC";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    SessionData sd = new SessionData();
                    sd.userId = uId;
                    sd.siteId = siteId;
                    sd.session_id = SQLReader.GetInt32(0);
                    sd.plan_name = SQLReader.GetString(1);
                    sd.start_time = SQLReader.GetString(2);
                    sd.practice_count = SQLReader.GetInt32(3);
                    sd.plan_id = SQLReader.GetInt32(4);
                    uSessionInfo.Add(sd);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getUserSessionInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return uSessionInfo;
        }

        public SessionInfo getPatientSessionInfo(long userId, int session, int site) {
            SqlConnection conn = new SqlConnection();
            SessionInfo sessionInfo = new SessionInfo();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT * FROM [dbo].session_data WHERE user_id = " + userId + " AND site_id = " + site + " AND session_id = " + session;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        sessionInfo.session_id = SQLReader.GetInt32(0);
                        sessionInfo.user_id = SQLReader.GetInt32(1);
                        sessionInfo.site_id = SQLReader.GetInt32(2);
                        sessionInfo.Date = SQLReader.GetString(3);
                        sessionInfo.Total_Distance = Convert.ToSingle(SQLReader.GetDouble(4));
                        sessionInfo.Step_Count = (int)Convert.ToSingle(SQLReader.GetDouble(5));
                        sessionInfo.Session_Time_gross = Convert.ToSingle(SQLReader.GetDouble(6));
                        sessionInfo.Speed = Convert.ToSingle(SQLReader.GetDouble(7));
                        sessionInfo.Step_hight_Right = Convert.ToSingle(SQLReader.GetDouble(8));
                        sessionInfo.Step_hight_Left = Convert.ToSingle(SQLReader.GetDouble(9));
                        sessionInfo.Step_Length_Right = Convert.ToSingle(SQLReader.GetDouble(10));
                        sessionInfo.Step_Length_Left = Convert.ToSingle(SQLReader.GetDouble(11));
                        sessionInfo.Movement_Time = Convert.ToSingle(SQLReader.GetDouble(12));
                    }
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientSessionInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return sessionInfo;
        }

        public void getPatientSessionInfoSql(long userId, int session, int site)
        {

        }

        public ArrayList getUserSessionInfo(long userId, int siteId, int DataLimit, int sessionId) {
            SqlConnection conn = new SqlConnection();
            ArrayList siArray = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT * FROM (SELECT TOP (" + DataLimit + ") * FROM [dbo].session_data WHERE user_id = " +
                    userId + "AND site_id = " + siteId + " AND session_id <= " + sessionId + " order by session_id DESC ) as tbl ORDER BY tbl.session_id";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    SessionInfo sf = new SessionInfo();
                    sf.session_id = SQLReader.GetInt32(0);
                    sf.user_id = SQLReader.GetInt32(1);
                    sf.site_id = SQLReader.GetInt32(2);
                    sf.Date = SQLReader.GetString(3);
                    sf.Total_Distance = Convert.ToSingle(SQLReader.GetDouble(4));
                    sf.Step_Count = (int)Convert.ToSingle(SQLReader.GetDouble(5));
                    sf.Session_Time_gross = Convert.ToSingle(SQLReader.GetDouble(6));
                    sf.Speed = Convert.ToSingle(SQLReader.GetDouble(7));
                    sf.Step_hight_Right = Convert.ToSingle(SQLReader.GetDouble(8));
                    sf.Step_hight_Left = Convert.ToSingle(SQLReader.GetDouble(9));
                    sf.Step_Length_Right = Convert.ToSingle(SQLReader.GetDouble(10));
                    sf.Step_Length_Left = Convert.ToSingle(SQLReader.GetDouble(11));
                    sf.Movement_Time = Convert.ToSingle(SQLReader.GetDouble(12));
                    siArray.Add(sf);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getUserSessionInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return siArray;
        }

        public ArrayList getParkinsonStepsData(long userId, int siteId, int sessionId) {
            SqlConnection conn = new SqlConnection();
            ArrayList pStepsArray = new ArrayList();
            string query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT step_type, step_length, step_time, pratice_stage, practice_pass, step_num_in_stage, stage_type" +
                        " FROM [dbo].[parkinson_steps_log],[dbo].[practice_log]" +
                        " WHERE parkinson_steps_log.practice_number = practice_log.practice_num" +
                        " AND   practice_log.session_id = " + sessionId +
                        " AND   practice_log.practice_userId = " + userId + 
                        " AND   practice_log.site_id = " + siteId +
                        " order by stage_type, step_num_in_stage;";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    ParkinsonStepsData sd = new ParkinsonStepsData();
                    sd.step_type = SQLReader.GetInt32(0);
                    sd.step_length = Convert.ToSingle(SQLReader.GetDouble(1));
                    sd.step_time = Convert.ToSingle(SQLReader.GetDouble(2));
                    sd.pratice_stage = SQLReader.GetInt32(3);
                    sd.practice_pass = SQLReader.GetInt32(4);
                    sd.step_num_in_stage = SQLReader.GetInt32(5);
                    sd.stage_type = SQLReader.GetInt32(6);
                    pStepsArray.Add(sd);
                }

            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getParkinsonSessionInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return pStepsArray;
        }

        public ArrayList getParkinsonSessionInfo(long userId, int siteId, int sessionId) {
            SqlConnection conn = new SqlConnection();
            ArrayList pDataArray = new ArrayList();
            string query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT MAX(practice_number) AS PracticeNumber" +
                        " ,AVG(stage_type) As PracticeStage" +
                        " ,COUNT(pratice_stage) AS StepCount" +
                        " ,AVG(step_length) AS AvgStepTotalLength" +
                        " ,STDEV(step_length) AS StdDeviation" +
                        " ,SUM(step_time) AS StepTime" +
                        " FROM [dbo].parkinson_steps_log,[dbo].practice_log" +
                        " WHERE parkinson_steps_log.practice_number = practice_log.practice_num" +
                        " AND practice_log.session_id = " + sessionId +
                        " AND practice_log.practice_userId = " + userId +
                        " AND practice_log.site_id = " + siteId +
                        " AND step_type in (10,20) GROUP BY pratice_stage;";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    ParkinsonSessionData pd = new ParkinsonSessionData();                    
                    pd.practice_id = SQLReader.GetInt32(0);
                    pd.PracticeStage = SQLReader.GetInt32(1);
                    pd.stepCount = SQLReader.GetInt32(2);
                    pd.AvgStepTotalLength = Convert.ToSingle(SQLReader.GetDouble(3));
                    pd.StdDeviation = Convert.ToSingle(SQLReader.GetDouble(4));
                    pd.StepTime = Convert.ToSingle(SQLReader.GetDouble(5));
                    pDataArray.Add(pd);
                }                
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getParkinsonSessionInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return pDataArray;
        }

        public List<SessionPractices> getSessionPractices(int site, int session_id) {
            SqlConnection conn = new SqlConnection();
            List<SessionPractices> spArray = new List<SessionPractices>();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT * FROM[dbo].session_practices_view where session_id = " + session_id + "AND site_id = " + site;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();

                SessionPractices previousPractice = null;

                while (SQLReader.Read()) {
                    SessionPractices sp = new SessionPractices();
                    sp.session_id = SQLReader.GetInt32(0);
                    sp.userId = SQLReader.GetInt32(1);
                    sp.siteId = SQLReader.GetInt32(2);
                    sp.practice_id = SQLReader.GetInt32(3);
                    sp.unity_id = SQLReader.GetInt32(4);
                    sp.practice_name = SQLReader.GetString(5);
                    sp.reps = 1;

                    if (previousPractice == null)
                    {
                        previousPractice = sp;
                    }
                    else if(sp.practice_id == previousPractice.practice_id) 
                    {
                        previousPractice.reps++;
                    }
                    else
                    {
                        spArray.Add(previousPractice);
                        previousPractice = sp;
                    }
                }
                spArray.Add(previousPractice);
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getSessionPractices: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return spArray;
        }

        public PlanDataSlimWithAdditionalInfo updatePlan(int plan_id, int patient_id, int site_id, string plan_comment, bool force_create, List<PlanDataSlim> planDataSlimList)
        {
            if(planDataSlimList == null || planDataSlimList.Count == 0)
            {
                return null;
            }

            if(plan_comment == null)
            {
                plan_comment = "";
            }

            planDataSlimList.Sort((x, y) => x.order_num.CompareTo(y.order_num));

            // get the original exercises in plan with this plan_id in db
            List<PlanData> planDataArrayList = getUserPlanData(plan_id, site_id, 0);

            bool needToCreateNewPlan = false;

            if(planDataArrayList.Count == 0)
            {
                needToCreateNewPlan = true;
            }
            else
            {
                // check if the exercises are identical
                if(planDataArrayList.Count == planDataSlimList.Count)
                {
                    for(int i = 0; i < planDataArrayList.Count; i++)
                    {
                        PlanData existingPlanData = (PlanData)planDataArrayList[i];
                        PlanDataSlim newPlanData = planDataSlimList[i];

                        if(existingPlanData.practice_unity_id != newPlanData.practice_id ||
                            existingPlanData.reps != newPlanData.reps)
                        {
                            needToCreateNewPlan = true;
                            break;
                        }
                    }
                }
                else
                {
                    needToCreateNewPlan = true;
                }
            }

            int new_plan_id = plan_id;
            bool new_plan_required = false;

            if(needToCreateNewPlan == false)
            {
                // the exercises and reps are the same, just update the comment
                postPlanComment(plan_id, site_id, plan_comment);
            }
            else
            {
                if (force_create == true)
                {
                    new_plan_id = storePatientPlan(patient_id, site_id, "Plan ", planDataSlimList, plan_comment);
                }
                else
                {
                    new_plan_required = true;
                }
            }
            
            return new PlanDataSlimWithAdditionalInfo(new_plan_id, plan_comment, new_plan_required, planDataSlimList);
        }

        public List<PlanData> getUserPlanData(int plan, int site, int lang_id) {
            SqlConnection conn = new SqlConnection();
            List<PlanData> userPdata = new List<PlanData>();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();

                SqlDataReader SQLReader = null;
                query = "SELECT practice_data.practice_id, practice_names.practice_name, plans.practice_id, plans.reps " +
                        " FROM [dbo].plans, [dbo].practice_data, [dbo].practice_names " +
                        " WHERE plan_number = " + plan +
                        " AND site_id = " + site +
                        " AND plans.practice_id = practice_data.unity_id" + //TO DO: " AND plans.practice_id = practice_data.practice_id" +
                        " AND practice_data.is_active = 1" +
                        " AND practice_names.lang_id = " + lang_id +
                        " AND practice_data.practice_id = practice_names.practice_id" + 
                        " ORDER BY order_num";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PlanData pd = new PlanData();
                    pd.practice_id = SQLReader.GetInt32(0);
                    pd.practice_name = SQLReader.GetString(1);
                    pd.practice_unity_id = SQLReader.GetInt32(2);
                    pd.reps = SQLReader.GetInt32(3);
                    userPdata.Add(pd);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getUserPlanData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return userPdata;
        }

        public int storePatientPlan(long patient, int site, string name, List<PlanDataSlim> pData, string comment) {
            // 1. For first plan in the list (0):
            // new_plan# = exec add_new_plan @patientId int, @siteId int, @practice_uId int, @order int, @reps int
            // 
            // 2. For each of the rest of the list (>0):
            //  exec add_to_plan @patientId int, @siteId int, @practice_uId int, @order int, @reps int, <new_plan#>
            // Return count of inserted plans
            SqlConnection conn = new SqlConnection();
            //SqlDataReader SQLReader = null;
            SqlCommand cmd = null;

            int pCount = 0;
            int planId = 0;
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                foreach (PlanDataSlim pd in pData) {
                    if (pCount == 0) {
                        cmd = new SqlCommand("add_new_plan", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@patientId", patient);
                        cmd.Parameters.AddWithValue("@siteId", site);
                        cmd.Parameters.AddWithValue("@practice_uId", pd.practice_id);
                        cmd.Parameters.AddWithValue("@order", pd.order_num);
                        cmd.Parameters.AddWithValue("@reps", pd.reps);
                        SqlParameter ret = new SqlParameter("@return_value", SqlDbType.Int);
                        ret.Direction = ParameterDirection.ReturnValue;
                        cmd.Parameters.Add(ret);
                        cmd.ExecuteNonQuery();
                        planId = (int)cmd.Parameters["@return_value"].Value;
                        if (planId <= 0) {
                            return -1;
                        }
                    }
                    else {
                        cmd = new SqlCommand("add_to_plan", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@patientId", patient);
                        cmd.Parameters.AddWithValue("@siteId", site);
                        cmd.Parameters.AddWithValue("@practice_uId", pd.practice_id);
                        cmd.Parameters.AddWithValue("@order", pd.order_num);
                        cmd.Parameters.AddWithValue("@reps", pd.reps);
                        cmd.Parameters.AddWithValue("@plan_num", planId);
                        SqlParameter ret = new SqlParameter("@return_value", SqlDbType.Int);
                        ret.Direction = ParameterDirection.ReturnValue;
                        cmd.Parameters.Add(ret);
                        cmd.ExecuteNonQuery();
                        int result = (int)cmd.Parameters["@return_value"].Value;
                        if (result == 2 || result == 1) { //2 = no such plan #, 1 = insert error
                            return -2;
                        }
                    }
                    pCount++;
                }
                if (pCount > 0) {  // if we arrived here, all was stored
                    addPlanInfo(planId, name, patient, site, comment);
                }
            }
            catch (SqlException ex) {
                if (cmd != null) {
                    addExceptionLog(cmd.Parameters.ToString(), "Manager|storePatientPlan: " + ex.Message);
                }
                else {
                    addExceptionLog("cmd is null", "Manager|storePatientPlan: " + ex.Message);
                }
                throw ex;
            }
            finally {
                conn.Close();
            }
            return planId;
        }

        public int getPatientPlanCount(long pId) {
            SqlConnection conn = new SqlConnection();
            int pCount = 0;
            string query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT COUNT(*) FROM [dbo].plans_info WHERE plan_user_id = " + pId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        //MessageBox.Show("Count = " + count);
                        pCount = int.Parse(SQLReader[0].ToString());
                    }
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientPlanCount: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return pCount;
        }

        public List<PlanInfo> getUserPlansInfo(long uId, int sId) {
            SqlConnection conn = new SqlConnection();
            List<PlanInfo> userPinfo = new List<PlanInfo>();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT plans_info.plan_number, plans_info.plan_name, COUNT(plans.plan_id) AS count, plans_info.comment, plans_info.creation_date " +
                                " FROM [dbo].plans_info INNER JOIN [dbo].plans " +
                                " ON(plans_info.plan_number = plans.plan_number) " +
                                " WHERE plans_info.plan_user_id in (" + uId + ", -2) AND plans_info.site_id = " + sId + "AND plans.site_id = " + sId +
                                " GROUP BY plans.plan_number, plans_info.plan_number, plans_info.plan_name, plans_info.comment, plans_info.creation_date"; 
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PlanInfo pi = new PlanInfo();
                    pi.plan_number = SQLReader.GetInt32(0);
                    pi.plan_name = SQLReader.GetString(1);
                    pi.count = SQLReader.GetInt32(2);

                    pi.plan_comment = SQLReader["comment"].ToString();
                    pi.creation_date = SQLReader["creation_date"].ToString();

                    userPinfo.Add(pi);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getUserPlansInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return userPinfo;
        }

        public ArrayList getPracticesInfo(int siteId, int langId) {
            SqlConnection conn = new SqlConnection();
            ArrayList practices_info = new ArrayList();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT * " +
                    " FROM practice_info" +
                    " WHERE lang_id = " + langId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeInfo pi = new PracticeInfo();

                    pi.practice_id = int.Parse(SQLReader["practice_id"].ToString());
                    pi.practice_description = SQLReader["Practice_description"].ToString();
                    pi.main_focus = SQLReader["main_focus"].ToString();
                    //pi.practice_instructions = SQLReader["practice_instructions"].ToString();
                    //pi.changeable_parameters = SQLReader["changeable_parameters"].ToString();
                    //pi.physical_benefits = SQLReader["physical_benefits"].ToString();

                    practices_info.Add(pi);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPracticesInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return practices_info;
        }

        public PlanSummary getPatientPlanSummaryData(long pId, int sId) {
            SqlConnection conn = new SqlConnection();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                PlanSummary ps = new PlanSummary();
                SqlDataReader SQLReader = null;
                query = "SELECT count(distinct plan_number) AS plan_count, sum(reps) AS reps FROM plans " +
                                " WHERE plan_user = " + pId + " AND site_id = " + sId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) {
                    ps.plan_count = SQLReader.GetInt32(0);
                    ps.reps = SQLReader.GetInt32(1);
                    return ps;
                }
                else {
                    return null;
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getUserPlansInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
        }

        public ArrayList getImageNames(int site) {
            SqlConnection conn = new SqlConnection();
            ArrayList ImageData = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT practice_id, image_id FROM [dbo].practice_data WHERE is_active = 1";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    ImageData id = new ImageData();
                    id.practice_id = SQLReader.GetInt32(0);
                    id.image_id = SQLReader.GetInt32(1);
                    ImageData.Add(id);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getImageNames: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return ImageData;
        }

        public ArrayList getPerformedPractices(long pId, int sId, int lang_id) {
            SqlConnection conn = new SqlConnection();
            ArrayList PracticePerformedData = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT MAX(practice_data.[practice_id]) as practicId, " +
                        " ( SELECT TOP 1 [practice_name] " +
                        "       FROM [dbo].[practice_names] " +
                        "       WHERE [practice_id] = practice_data.[practice_id]   " +
                        "       AND [dbo].[practice_names].lang_id = " + lang_id + ") as practiceName " +
	                    " , COUNT([session_id]) as performed"+
                        " FROM [dbo].[practice_log], [dbo].[practice_data] "+
                        " WHERE practice_data.[practice_id] = practice_log.practice_id "+
                        " AND practice_userId = " + pId + 
                        " AND site_id = " + sId +
                        " GROUP BY practice_data.[practice_id]";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PerformedPractices px = new PerformedPractices();
                    px.practice_id = SQLReader.GetInt32(0); 
                    px.practiceName = SQLReader.GetString(1);
                    px.performed = SQLReader.GetInt32(2);
                    PracticePerformedData.Add(px);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPerformedPractices: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return PracticePerformedData;
        }

        /// <summary>
        /// IT IS USED IN THE OLD MANAGER. WILL BE DELETED WHEN THE OLD MANAGER WILL BE DISSMISSED
        /// </summary>
        /// <returns></returns>
        public ArrayList getPracticeSupportedMeasures() {
            SqlConnection conn = new SqlConnection();
            ArrayList PracticsMeasures = new ArrayList();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT [practice_supported_measures].[practice_id],[practice_measures].[measure_id],[measure_name]"+
                        " FROM [dbo].[practice_measures], [dbo].[practice_supported_measures]"+
                        " WHERE [practice_measures].[measure_id] = [practice_supported_measures].[measure_id]";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeMeasures pm = new PracticeMeasures();
                    pm.practice_id = SQLReader.GetInt32(0);
                    pm.measure_id = SQLReader.GetInt32(1);
                    pm.measure_name = SQLReader.GetString(2);
                    PracticsMeasures.Add(pm);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPracticeSupportedMeasures: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return PracticsMeasures;
        }

        /// <summary>
        /// USED IN THE NEW REACT MANAGER. getPracticeSupportedMeasures() WILL BE DELETED
        /// </summary>
        /// <returns></returns>
        public ArrayList getPracticeSupportedMeasures(int lang_id) {
            SqlConnection conn = new SqlConnection();
            ArrayList PracticsMeasures = new ArrayList();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT [practice_supported_measures].[practice_id], [practice_measures_new].[measure_id], [practice_measures_new].[measure_name]" +
                        " FROM [dbo].[practice_measures_new], [dbo].[practice_supported_measures]" +
                        " WHERE [practice_measures_new].[measure_id] = [practice_supported_measures].[measure_id]" +
                        " AND [practice_measures_new].[lang_id] = " + lang_id;

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeMeasures pm = new PracticeMeasures();
                    pm.practice_id = SQLReader.GetInt32(0);
                    pm.measure_id = SQLReader.GetInt32(1);
                    pm.measure_name = SQLReader.GetString(2);
                    PracticsMeasures.Add(pm);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPracticeSupportedMeasures(lang_id): " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return PracticsMeasures;
        }

        // for the old wpf manager system
        public ArrayList getPracticeMeasuresData(long Pid, int site, int practice_id) {
            SqlConnection conn = new SqlConnection();
            ArrayList pMeasuresLog = new ArrayList();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT practice_id,practice_name,practice_number,measure_id,measure_name,measure_score_log,session_id,convert(varchar,end_time,4) as Date" +
                        " FROM[dbo].[practice_measures_view]" +
                        " WHERE user_id = " + Pid +
                        " AND site_id = " + site +
                        " AND practice_id = "+ practice_id;

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeMeasuresLog ml = new PracticeMeasuresLog();      
                    ml.practice_id = SQLReader.GetInt32(0);
                    ml.practiceName = SQLReader.GetString(1);
                    ml.practiceNumber = SQLReader.GetInt32(2);
                    ml.measureId = SQLReader.GetInt32(3);
                    ml.measureName = SQLReader.GetString(4);
                    ml.measureScoreLog = float.Parse(SQLReader[5].ToString());
                    ml.sessionId = SQLReader.GetInt32(6);
                    ml.practiceTime = SQLReader.GetString(7);
                    pMeasuresLog.Add(ml);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPracticeMeasuresData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return pMeasuresLog;
        }

        // for the new react manager system with diffrenet languages
        public ArrayList getPracticeMeasuresDataWithLang(long user_id, int site_id, int practice_id, int lang_id) {
            SqlConnection conn = new SqlConnection();
            ArrayList pMeasuresLog = new ArrayList();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                query = " SELECT practice_measure_log.practice_id, dbo.practice_names.practice_name, dbo.practice_measure_log.practice_number, " +
                                    " practice_measure_log.measure_id, dbo.practice_measures_new.measure_name, dbo.practice_measure_log.measure_score_log, " +
                                    " practice_measure_log.site_id, practice_measure_log.user_id, practice_measure_log.session_id, convert(varchar, sessions.end_time, 4) as Date " +
                               " FROM dbo.practice_measure_log, " +
                                    " dbo.practice_measures_new, " +
                                    " dbo.practice_names, " +
                                    " dbo.sessions " +
                               " WHERE dbo.practice_measure_log.user_id = " + user_id +
                                     " AND dbo.practice_measure_log.practice_id = " + practice_id +
                                     " AND dbo.practice_measure_log.site_id = " + site_id +
                                     " AND dbo.practice_names.practice_id = " + practice_id +
                                     " AND dbo.practice_names.lang_id = " + lang_id +
                                     " AND dbo.practice_measures_new.lang_id = " + lang_id +
                                     " AND dbo.sessions.user_id = " + user_id +
                                     " AND dbo.sessions.site_id = " + site_id +

                                  " AND (practice_measure_log.measure_id = practice_measures_new.measure_id) " +
                                  " AND (practice_names.practice_id = practice_measure_log.practice_id) " +
                                  " AND (sessions.session_id = practice_measure_log.session_id)";


                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeMeasuresLog ml = new PracticeMeasuresLog();
                    
                    ml.practice_id = int.Parse(SQLReader["practice_id"].ToString());
                    ml.practiceName = SQLReader["practice_name"].ToString();
                    ml.practiceNumber = int.Parse(SQLReader["practice_number"].ToString()); 
                    ml.measureId = int.Parse(SQLReader["measure_id"].ToString()); 
                    ml.measureName = SQLReader["measure_name"].ToString(); 
                    ml.measureScoreLog = float.Parse(SQLReader["measure_score_log"].ToString()); 
                    ml.sessionId = int.Parse(SQLReader["session_id"].ToString());
                    ml.practiceTime = SQLReader["Date"].ToString(); 
                    pMeasuresLog.Add(ml);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPracticeMeasuresDataWithLang: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return pMeasuresLog;
        }



        public ArrayList getAllActivePractices(int lang_id) {
            SqlConnection conn = new SqlConnection();
            ArrayList PracticeData = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT practice_data.practice_id, practice_names.practice_name, practice_data.unity_id " +
                        " FROM [dbo].practice_data, [dbo].practice_names " +
                        " WHERE practice_data.is_active = 1 " +
                        " AND practice_data.unity_id > -1 " +
                        " AND practice_names.lang_id = " + lang_id + " " +
                        " AND practice_data.practice_id = practice_names.practice_id";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeData pd = new PracticeData();
                    pd.practice_id = SQLReader.GetInt32(0);
                    pd.practice_name = SQLReader.GetString(1);
                    pd.unity_id = SQLReader.GetInt32(2);
                    PracticeData.Add(pd);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getAllActivePractices: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return PracticeData;
        }

        public int getQuickPlan(long patient, int site, int totalTime, bool standing, bool sitting, List<string> categories) {
            SqlConnection conn = new SqlConnection();            
            Random rnd = new Random();
            List<List<int>> practiceList = new List<List<int>>();
            int planId = -1;

            // set the plan name
            string planName = "Plan #" + (1+ getPatientPlanCount(patient)).ToString();

            // prepare plan data
            List<PlanData> planData = new List<PlanData>();
            int order = 0;

            foreach (string cat in categories) {
                // get the list of practices for the category
                practiceList.Add(getFilteredPractices(1, cat, totalTime));
            }

            // create filter list for each category + plan list            
            List<string> filterCat = new List<string>(categories);
            int timeDestination = 0;

            // 1. while time < totalTime
            while (timeDestination < totalTime) {
                // 2. make copy list of categories
                if (filterCat.Count == 0) {
                    filterCat = new List<string>(categories);
                }

                // 3. random select one category and remove it from list
                int selFilter = 0;
                if (filterCat.Count > 1)
                    selFilter = rnd.Next(1, filterCat.Count + 1) - 1;
                string selFilteStr = filterCat[selFilter];
                filterCat.RemoveAt(selFilter);
                // 5. random select from filter list, and pop selected
                int selPractice = 0;
                if (practiceList[selFilter].Count > 1)
                    selPractice = rnd.Next(1, practiceList[selFilter].Count + 1) - 1;
                

                // 6. save selected in plan list
                PlanData p = new PlanData();
                p.practice_unity_id = practiceList[selFilter][selPractice];
                p.order_num = order;
                order++;
                p.reps = 1;
                planData.Add(p);
                practiceList[selFilter].RemoveAt(selPractice);
                timeDestination++;
            }

            planId = storePatientPlan(patient, site, planName, PlanDataSlim.GetPlanDataSlimList(planData), "");

            // store the plan and return the new plan ID

            return planId;
        }

        public string getPlanComment(int planId, int siteId) {

            SqlConnection conn = new SqlConnection();

            string comment = "";

            string query = string.Empty;
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                query = "SELECT comment " +
                        " FROM plans_info" +
                        " WHERE plan_number = " + planId + 
                                " AND site_id = " + siteId;

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();

                List<SessionDone> sessionsDone = new List<SessionDone>();

                if (SQLReader.HasRows)
                    while (SQLReader.Read()) {
                        // Check for NULL value in the "comment" column
                        comment = SQLReader.IsDBNull(0) ? "" : SQLReader.GetString(0);

                    }
                SQLReader.Dispose();
                
            }
            catch (Exception ex) {
                addExceptionLog(query, "Manager|getPlanComment: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            

            return comment;

        }

        public bool postPlanComment(int planId, int siteId, string comment) {
            if (comment == null)
                return false;

            SqlConnection conn = new SqlConnection();
            SqlDataReader SQLReader = null;
            string query = "";
            //int level = 0;
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "SELECT * FROM [dbo].plans_info WHERE plan_number= " + planId + " AND site_id = " + siteId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) { // If plan exits
                    SQLReader.Close();
                    query = "UPDATE [dbo].plans_info SET" +
                            " comment = '" + comment + "' " + 
                            " WHERE plan_number = " + planId +
                            " AND site_id = " + siteId;
                    cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|postPlanComment: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
        }

        public bool updatePlanName(int plan_id, int patient_id, int site_id, string new_plan_name)
        {
            if (new_plan_name == null || new_plan_name == "")
                return false;

            bool planNameAlreadyTaken = isPlanNameExists(new_plan_name, patient_id, site_id);

            if (planNameAlreadyTaken == true)
            {
                return false;
            }

            SqlConnection conn = new SqlConnection();
            SqlDataReader SQLReader = null;
            string query = "";
            try
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "SELECT * FROM [dbo].plans_info WHERE plan_number = " + plan_id + " AND site_id = " + site_id;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read())
                { // If plan exits
                    SQLReader.Close();
                    query = "UPDATE [dbo].plans_info SET" +
                            " plan_name = '" + new_plan_name + "' " +
                            " WHERE plan_number = " + plan_id +
                            " AND site_id = " + site_id;
                    cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException ex)
            {
                addExceptionLog(query, "Manager|updatePlanName: " + ex.Message);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        private List<int> getFilteredPractices(int pos, string filter, int top) {
            SqlConnection conn = new SqlConnection();
            List<int> retList = new List<int>();
            string query = string.Empty;
            try {
                query = "SELECT TOP ("+top+") * FROM [dbo].practice_filters_view WHERE practice_position = " + pos + " AND " + filter + " > 1 ORDER BY " + filter + " DESC";
                
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {                    
                    retList.Add(SQLReader.GetInt32(2));
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getFilteredPractices: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return retList;
        }

        public ArrayList getFilterActivePractices(int pos, List<string> filters) {
            SqlConnection conn = new SqlConnection();
            ArrayList PracticeData = new ArrayList();
            string query = string.Empty;
            try {
                query = "SELECT TOP (1000) * FROM [dbo].practice_filters_view WHERE practice_position = " + pos;
                if (filters.Count > 0) {
                    int index = 1;
                    foreach (string fString in filters) {
                        // add the nested query on top of the existing
                        string nqName = "q" + index;
                        query = "SELECT TOP (1000) " + nqName + ".* FROM (" + query + ") " + nqName + " WHERE " + nqName + "." + fString + " > 1 order by " + nqName + "." + fString + " desc";

                        index++;
                    }
                }
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    PracticeData pd = new PracticeData();
                    pd.practice_id = SQLReader.GetInt32(2);
                    pd.practice_name = SQLReader.GetString(0);
                    pd.unity_id = SQLReader.GetInt32(2);
                    PracticeData.Add(pd);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getFilterActivePractices: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return PracticeData;
        }

        public GeneralInfo getGeneralInfo() {
            GeneralInfo gi = new GeneralInfo();
            SqlConnection conn = new SqlConnection();
            string query =  "SELECT (SELECT COUNT(*) AS activeSites FROM [dbo].[Sites]),"+
                            "(SELECT COUNT(*) AS totalPatients FROM [dbo].[patient_info]),"+
                            "(SELECT COUNT(*) AS activePlans FROM [dbo].[plans_info] WHERE plan_user_id > 0)," +
                            "(SELECT COUNT(*) performedSessions FROM [dbo].[sessions] WHERE end_time > start_time)," +
                            "(SELECT COUNT(*) AS performedExercises FROM[dbo].[practice_log])," +
                            "(SELECT SUM([Step_Count]) AS totalSteps  FROM[dbo].[session_data])," +
                            "(SELECT convert(varchar, dateadd(ms, (SUM([Session_Time_gross]) * 1000), 0),114) AS TotalTime_gross  FROM[dbo].[session_data])," +
                            "(SELECT SUM([Total_Distance]) AS Total_Distance  FROM[dbo].[session_data])," +
                            "(SELECT COUNT(*) AS execrsisesCount FROM [dbo].[practice_data] WHERE is_active = 1)";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows) {
                    while (SQLReader.Read()) {
                        //MessageBox.Show("Count = " + count);
                        gi.active_sites = int.Parse(SQLReader[0].ToString());
                        gi.total_patients = int.Parse(SQLReader[1].ToString());
                        gi.active_plans = int.Parse(SQLReader[2].ToString());
                        gi.performedSessions = int.Parse(SQLReader[3].ToString());
                        gi.performedExercises = int.Parse(SQLReader[4].ToString());
                        gi.totalSteps = int.Parse(SQLReader[5].ToString());
                        gi.TotalTime_gross = SQLReader[6].ToString();
                        gi.Total_Distance = float.Parse(SQLReader[7].ToString());
                        gi.TotalSystemExecrsises = int.Parse(SQLReader[8].ToString());
                    }
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientsCount: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return gi;
        }

        public ArrayList getGeneralReportDataFiltered(string from, string to, string pID, int site) {
            SqlConnection conn = new SqlConnection();
            ArrayList repData = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT * FROM [dbo].session_report " +
                        "WHERE Date between '" + from + "' " +
                        "AND '" + to + "' " +
                        "AND user_id like '" + pID + "' AND site_id = " + site;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    GeneralReportData gr = new GeneralReportData();
                    gr.session_id = SQLReader.GetInt32(0);
                    gr.user_id = SQLReader.GetInt32(1);
                    gr.exercises = SQLReader.GetInt32(3);
                    gr.date = SQLReader.GetString(4);
                    gr.start_time = SQLReader.GetString(5);
                    gr.end_time = SQLReader.GetString(6);
                    gr.duration = SQLReader.GetInt32(7);
                    gr.site_id = SQLReader.GetInt32(2);
                    repData.Add(gr);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getGeneralReportDataFiltered: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return repData;
        }

        public ArrayList getSummaryReportDataFiltered(string from, string to, string uIdStr, int siteId) {
            SqlConnection conn = new SqlConnection();
            ArrayList summaryData = new ArrayList();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT COUNT(session_id) AS SessionCount, COUNT(distinct user_id) as Patients,SUM(Exercises) AS TotalExercises," +
                                " SUM(Duration) AS TotalSeconds, MIN(Date) AS FromDate, MAX(Date) AS ToDate," +
                                " DATEDIFF(MONTH, MIN(Date), MAX(Date))  AS TotalMonth" +
                                " FROM[dbo].session_report WHERE Date between '" + from + "' AND '" + to + "'" +
                                " AND user_id like '" + uIdStr + "'" +
                                " AND site_id = " + siteId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    SummaryReportData srp = new SummaryReportData();
                    srp.session_count = SQLReader.GetInt32(0);
                    srp.patients = SQLReader.GetInt32(1);
                    srp.total_exercises = SQLReader.GetInt32(2);
                    srp.total_seconds = SQLReader.GetInt32(3);
                    srp.start_time = SQLReader.GetString(4);
                    srp.end_time = SQLReader.GetString(5);
                    srp.total_months = SQLReader.GetInt32(6) + 1;
                    

                    srp.completion_percentage = getAvgSessionCompletionPercentage(from, to, uIdStr, siteId);
                    summaryData.Add(srp);



                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getSummaryReportDataFiltered: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return summaryData;
        }

        

        

        private List<PlanInfoWithAllPracticesAndReps> getAllPlansPracticesAndReps(List<SessionDone> sessionsDone, int site_id) {

            List<PlanInfoWithAllPracticesAndReps> allPlansInfo = new List<PlanInfoWithAllPracticesAndReps>();

            SqlConnection conn = new SqlConnection();

            string query = string.Empty;
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                string plan_ids_str = "";

                foreach (SessionDone sessionDone in sessionsDone) {

                    if (plan_ids_str.Equals("") == false) {
                        plan_ids_str += ",";
                    }

                    plan_ids_str += sessionDone.plan_id;
                }

                if (plan_ids_str.Equals(""))
                    return allPlansInfo;

                query = "SELECT plan_number, practice_id , reps FROM plans WHERE" +
                                            " site_id = " + site_id +
                                            " AND plan_number IN (" + plan_ids_str + ")";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();


                if (SQLReader.HasRows)
                    while (SQLReader.Read()) {
                        // Fill in the dataset

                        int plan_id = int.Parse(SQLReader["plan_number"].ToString());
                        int practice_id = int.Parse(SQLReader["practice_id"].ToString());
                        int reps = int.Parse(SQLReader["reps"].ToString());

                        bool needNewPlanObj = true;

                        PracticeAndRepsInPlan practiceAndRepsInPlan = new PracticeAndRepsInPlan(practice_id, reps);

                        foreach (PlanInfoWithAllPracticesAndReps planInfo in allPlansInfo) {
                            if (planInfo.plan_id == plan_id) {

                                needNewPlanObj = false;


                                planInfo.allPractices.Add(practiceAndRepsInPlan);
                            }
                        }

                        if (needNewPlanObj == true) {
                            PlanInfoWithAllPracticesAndReps newPlanInfo = new PlanInfoWithAllPracticesAndReps(plan_id);
                            newPlanInfo.allPractices.Add(practiceAndRepsInPlan);

                            allPlansInfo.Add(newPlanInfo);
                        }
                    }
                SQLReader.Dispose();

            }
            catch (Exception ex) {
                addExceptionLog(query, "Manager|getAllPlansPracticesAndReps: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return allPlansInfo;
        }

        private List<SessionPracticesLog> getAllSessionsPracticesLogs(List<SessionDone> sessionsDone, int site_id) {

            List<SessionPracticesLog> allSessionPracticesLogs = new List<SessionPracticesLog>();

            SqlConnection conn = new SqlConnection();

            string query = string.Empty;
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                string session_ids_str = "";

                foreach (SessionDone sessionDone in sessionsDone) {

                    if (session_ids_str.Equals("") == false) {
                        session_ids_str += ",";
                    }

                    session_ids_str += sessionDone.session_id;
                }

                if (session_ids_str.Equals(""))
                    return allSessionPracticesLogs;

                query = "SELECT session_id, practice_id FROM practice_log WHERE" +
                                            " site_id = " + site_id +
                                            " AND session_id IN (" + session_ids_str + ")";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();


                if (SQLReader.HasRows)
                    while (SQLReader.Read()) {
                        // Fill in the dataset

                        int session_id = int.Parse(SQLReader["session_id"].ToString());
                        int practice_id = int.Parse(SQLReader["practice_id"].ToString());

                        bool needNewSessionObj = true;


                        foreach (SessionPracticesLog sessionLog in allSessionPracticesLogs) {
                            if (sessionLog.session_id == session_id) {

                                needNewSessionObj = false;


                                sessionLog.practice_ids.Add(practice_id);
                            }
                        }

                        if (needNewSessionObj == true) {
                            SessionPracticesLog sessionLog = new SessionPracticesLog(session_id);
                            sessionLog.practice_ids.Add(practice_id);

                            allSessionPracticesLogs.Add(sessionLog);
                        }
                    }
                SQLReader.Dispose();

            }
            catch (Exception ex) {
                addExceptionLog(query, "Manager|getAllSessionsPracticesLogs: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }



            foreach(SessionDone sessionDone in sessionsDone) {

                foreach(SessionPracticesLog session in allSessionPracticesLogs) {

                    if(sessionDone.session_id == session.session_id) {
                        session.plan_id = sessionDone.plan_id;
                    }
                }
            }
            return allSessionPracticesLogs;
        }


        public TaskData setTaskData(int site, int machineId, string task_info) {
            TaskData data = new TaskData();
            int res = getTaskStatus(site, machineId);
            if (res == 1) {
                data.state = 4;
                data.task_info = "Machine has pending task";
            }
            if (res == 2) {
                data.state = 4;
                data.task_info = "Machine is bussy";
            }
            if (res == 0) { //we can add task...
                SqlConnection conn = new SqlConnection();
                string query = "";
                try {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                    conn.Open();
                    query = "INSERT INTO [dbo].tasks(site_id,machine_id,task_info,task_status,status_info) " + 
                            "VALUES (" + site + "," + machineId + ", \'" + task_info + "\', 1, \'new task\')";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    data.state = 1;
                    data.task_info = "Task added";
                }
                catch (SqlException ex) {
                    addExceptionLog(query, "Manager|addPlanInfo: " + ex.Message);
                    throw ex;
                }
                finally {
                    conn.Close();
                }
            }

            return data;
        }

        public int setTaskCompleted(int site, int machineId, int taskId) {
            int res = -1;
            SqlConnection conn = new SqlConnection();
            string query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "UPDATE [dbo].tasks SET task_status = 3 WHERE task_id = " + taskId + " AND site_id = " + site + " AND machine_id = " + machineId;
                //query = "UPDATE [dbo].tasks SET task_status = 3, status_info = 'completed' WHERE task_id = " + taskId + " AND site_id = " + site + " AND machine_id = " + machineId;
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
                res = 1;
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|setTaskRunning: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return res;
        }

        public TaskData getTaskData(int site, int machineId) {
            SqlConnection conn = new SqlConnection();
            TaskData data = new TaskData();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT task_id, task_info, task_status,status_info FROM [dbo].tasks WHERE " +
                    "site_id = " + site + " AND machine_id = " + machineId + " AND task_status = 1 ";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) {
                    data.taskId = SQLReader.GetInt32(0);
                    data.task_data = SQLReader.GetString(1);
                    data.state = SQLReader.GetInt32(2);
                    //data.task_info = SQLReader.GetString(3);
                }
                if (data.state == 1) {
                    // set state to running
                    bool ret = setTaskRunning(data.taskId);
                    if (ret == false) {
                        data.state = 4;
                        data.task_info = "Could not set to task running!";
                    }
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getTaskData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            

            return data;
        }

        public ArrayList getPatientSummaryReportData(string uIdStr, int siteId) {
            SqlConnection conn = new SqlConnection();
            ArrayList summaryData = new ArrayList();
            String query = "";

            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT COUNT(session_id) AS SessionCount, " +
                        "COUNT(distinct user_id) as Patients, " +
                        "SUM(Exercises) AS TotalExercises, " +
                        "SUM(Duration) AS TotalSeconds, " +
                        "MIN(Date) AS FromDate, " +
                        "MAX(Date) AS ToDate, " +
                        "DATEDIFF(MONTH, MIN(Date),MAX(Date))  AS TotalMonth " +
                        "FROM [dbo].session_report " +
                        "WHERE user_id like '" + uIdStr + "'" + " AND site_id =" + siteId;

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read()) {
                    SummaryReportData srp = new SummaryReportData();
                    srp.session_count = SQLReader.GetInt32(0);
                    srp.patients = SQLReader.GetInt32(1);
                    srp.total_exercises = SQLReader.GetInt32(2);
                    srp.total_seconds = SQLReader.GetInt32(3);
                    srp.start_time = SQLReader.GetString(4);
                    srp.end_time = SQLReader.GetString(5);
                    srp.total_months = SQLReader.GetInt32(6) + 1;
                    
                    srp.completion_percentage = getAvgSessionCompletionPercentage(uIdStr, siteId);


                    summaryData.Add(srp);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientSummaryReportData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return summaryData;
        }

        private string secondsToStringTime(int seconds) {
            int min = 0;
            min = seconds / 60;
            seconds = seconds % 60;

            string time = "";
            if(min < 10) {
                time += "0";
            }
            time += min;

            time += ":";

            if(seconds < 10) {
                time += "0";
            }
            time += seconds;

            return time;
        }
        
        public PatientOverviewData getPatientOverviewData(int user_id, int site_id) {

            //first get the duration (number of months in the system), number of sessions made and avg session time
            SqlConnection conn = new SqlConnection();
            PatientOverviewData data = new PatientOverviewData();
            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                query = "select DATEDIFF(month, MIN(start_time) ," +
                    " MAX(end_time)) as total_months_system," +
                    " COUNT(session_id) as sessions, avg(datediff(ss, start_time, end_time)) as avg_session_time_sec" +
                    " from sessions " +
                    " where user_id = " + user_id +" AND site_id = " + site_id +" AND start_time != end_time";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) {
                    data.system_duration = int.Parse(SQLReader["total_months_system"].ToString()) + 1;
                    data.num_of_sessions = int.Parse(SQLReader["sessions"].ToString());

                    int avg_session_time_seconds = int.Parse(SQLReader["avg_session_time_sec"].ToString());

                    data.avg_session_time = secondsToStringTime(avg_session_time_seconds);
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getPatientOverviewData: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }


            //add the other data needed
            data.avg_practices_session = getAvgPracticesInSession(user_id, site_id);
            data.avg_practice_time = getAvgPracticeTime(user_id, site_id);

            data.completion_percentage = getAvgSessionCompletionPercentage(user_id.ToString(), site_id);

            return data;
        }

        private string getAvgSessionCompletionPercentage(string uIdStr, int site_id)
        {

            //"WHERE user_id like '" + uIdStr + "'" + " AND site_id =" + siteId;
            List<float> completions = new List<float>();

            SqlConnection conn = new SqlConnection();
            String query = "";

            try
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT completion " +
                        " FROM [dbo].sessions " +
                        " WHERE user_id like '" + uIdStr + "'" + " AND site_id =" + site_id;

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read())
                {
                    completions.Add(float.Parse(SQLReader["completion"].ToString()));
                }
            }
            catch (SqlException ex)
            {
                addExceptionLog(query, "Manager|getAvgSessionCompletionPercentage: " + ex.Message);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return getFloatAsString(completions.Average() * 100);
        }

        private string getAvgSessionCompletionPercentage(string start_date, string end_date, string uIdStr, int site_id)
        {

            //" FROM[dbo].session_report WHERE Date between '" + from + "' AND '" + to + "'" +
            List<float> completions = new List<float>();

            SqlConnection conn = new SqlConnection();
            String query = "";

            try
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;
                query = "SELECT completion " +
                        " FROM [dbo].sessions " +
                        " WHERE user_id like '" + uIdStr + "'" + 
                        " AND site_id =" + site_id +
                        " AND start_time between '" + start_date + "' AND '" + end_date + "'";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                while (SQLReader.Read())
                {
                    completions.Add(float.Parse(SQLReader["completion"].ToString()));
                }
            }
            catch (SqlException ex)
            {
                addExceptionLog(query, "Manager|getAvgSessionCompletionPercentage: " + ex.Message);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return getFloatAsString(completions.Average() * 100);
        }

        private string getFloatAsString(float f)
        {
            //with max 2 digits after the dot - if there are no digits after the dot then there will be no dot also.
            return f.ToString("0.##");
        }

        private string getAvgPracticesInSession(int user_id, int site_id) {
            
            SqlConnection conn = new SqlConnection();
            float avgPracticeInSession = 0;

            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                query = "select avg(Cast(exercises_session_count.session_group_count as Float))  as avg_exercises_session " +
                    " from " +
                    " (select session_id, count(session_id) as session_group_count " +
                    " from  practice_log " +
                    " where practice_ts != practice_EndTime AND practice_userId = " + user_id +" AND site_id = "+ site_id +
                    " group by session_id) as exercises_session_count";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) {
                    avgPracticeInSession = float.Parse(SQLReader["avg_exercises_session"].ToString());
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getAvgPracticesInSession: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            

            return avgPracticeInSession.ToString("0.00");
        }

        private string getAvgPracticeTime(int user_id, int site_id) {

            SqlConnection conn = new SqlConnection();
            int avgPracticeTime = 0;

            String query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                SqlDataReader SQLReader = null;

                query = "select avg(practice_log_times.practice_time) as avg_practice_time" +
                    " from " +
                    " (select DATEDIFF(ss, practice_log.practice_ts, practice_log.practice_EndTime) as practice_time " +
                    " from practice_log " +
                    " where practice_ts != practice_EndTime AND practice_log.practice_userId = " + user_id + " AND practice_log.site_id = " + site_id + ") as practice_log_times";

                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read()) {
                    avgPracticeTime = (int) (float.Parse(SQLReader["avg_practice_time"].ToString()));
                }
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getAvgPracticeTime: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }



            return secondsToStringTime(avgPracticeTime);
        }





        /// <summary>
        /// Internal use
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////
        private void addPlanInfo(int planId, string pName, long patientId, int siteId, string comment) {

            if (comment == null)
                comment = "";

            string tmpName = pName;
            bool exists;
            int idx = 0;
            while ((exists = isPlanNameExists(tmpName, patientId, siteId)) == true) {
                tmpName = pName + "#" + idx;
                idx++;
            }
            SqlConnection conn = new SqlConnection();
            string query = "";
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "INSERT INTO [dbo].plans_info(plan_number, plan_name, plan_user_id, comment, site_id) " +
                                "VALUES (" + planId + ", '" + tmpName + "', " + patientId + ", '" + comment + "', " + siteId + ")";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|addPlanInfo: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
        }

        private int getTaskStatus(int site, int machineId) {
            int res = 0;
            SqlConnection conn = new SqlConnection();
            string query = "";
            try {
                SqlDataReader SQLReader = null;
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "SELECT task_status FROM [dbo].tasks WHERE " +
                    "site_id = " + site + " AND machine_id = " + machineId + " AND task_status in(1,2)";
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.Read())
                    res = SQLReader.GetInt32(0);
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|getTaskStatus: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return res;
        }

        private bool isPlanNameExists(string name, long pId, int sId) {
            bool ret = false;
            SqlConnection conn = new SqlConnection();
            string query = "";
            try {
                SqlDataReader SQLReader = null;
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "SELECT * FROM [dbo].plans_info WHERE plan_name = '" + name + "' AND plan_user_id = " + pId + " AND site_id = " + sId;
                SqlCommand cmd = new SqlCommand(query, conn);
                SQLReader = cmd.ExecuteReader();
                if (SQLReader.HasRows)
                    ret = true;
            }
            catch (SqlException ex) {
                addExceptionLog(query, "Manager|isPlanNameExists: " + ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }
            return ret;
        }

        private bool setTaskRunning(int taskId) {
            bool ret = false;
            SqlConnection conn = new SqlConnection();            
            string query = "";
            
            try {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["localDB"].ConnectionString;
                conn.Open();
                query = "UPDATE [dbo].tasks SET task_status = 2 WHERE task_id = " + taskId;
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
                    ret = true;
                }
                catch (SqlException ex) {
                    addExceptionLog(query, "Manager|setTaskRunning: " + ex.Message);
                    throw ex;
                }
                finally {
                    conn.Close();
                }
            return ret;
        }

        /// </summary>
        private static string users_key { get; set; } = "a46a3132536fa34fa9680e7ac1699d32";
        /// <summary>
        ///
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Encrypt(string text) {
            using (var md5 = new MD5CryptoServiceProvider()) {
                using (var tdes = new TripleDESCryptoServiceProvider()) {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(users_key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;
                    using (var transform = tdes.CreateEncryptor()) {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public static string Decrypt(string cipher) {
            using (var md5 = new MD5CryptoServiceProvider()) {
                using (var tdes = new TripleDESCryptoServiceProvider()) {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(users_key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;
                    using (var transform = tdes.CreateDecryptor()) {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }
    }
}